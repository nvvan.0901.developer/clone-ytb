// Action Menu for Sidebar
// Active item 
let itemSidebars = document.querySelectorAll('.sidebar__item');
itemSidebars.forEach(itemSidebar => {
    itemSidebar.addEventListener("click", (e)=> {
        itemSidebars.forEach((ele)=>{
            ele.classList.remove("active");
        })
        itemSidebar.classList.add("active");
    })
});
//show item 


let btnShowItem = document.querySelector('#show__menu_sidebar');
btnShowItem.onclick = function () {
    itemSidebars.forEach(itemSidebar => {
        itemSidebar.classList.remove("hidden");
        // itemSidebar.classList.add("hidden");
    })
    this.classList.add("hidden")
    btnHiddenItem.classList.remove("hidden")
}
let btnHiddenItem = document.querySelector('#hidden__menu_sidebar');
btnHiddenItem.onclick = function () {
    itemSidebars.forEach(itemSidebar => {
       let listClassForitem = itemSidebar.classList.value;
       if(listClassForitem.includes("hide"))
       {
        itemSidebar.classList.add("hidden");
       }
    //   console.log( listClassForitem.value)
    })
    this.classList.add("hidden");
    btnShowItem.classList.remove("hidden");
}
// console.log(btnShowItem)

// ACTION SIDEBAR

let btnToggleSidebar = document.querySelector('.header__menu-btn');
let sidebar = document.querySelector('.sidebar');
let main = document.querySelector('.main');
let tagbar = document.querySelector('.tagbar');
let isOpen = false;
btnToggleSidebar.onclick   = function() {
    console.log(btnToggleSidebar);
    console.log(sidebar);
    if(!isOpen){
        sidebar.style.width = "0px";
        main.style.width = "calc(100% - 100px)";
        main.style.left = "100px";
        tagbar.style.width = "calc(100% - 100px)";
        tagbar.style.left = "100px";
        isOpen = true;
    } else {
        sidebar.style.width = "240px";
        main.style.width = "calc(100% - 240px)";
        main.style.left = "240px";
        tagbar.style.width = "calc(100% - 240px)";
        tagbar.style.left = "240px";
        isOpen = false
    }

}
// Tagbar action

let btnPrevtagbar = document.querySelector(".pre__tagbar");
let btnNexttagbar = document.querySelector(".next__tagbar");
let tagbar__list = document.querySelector('.tabbar__list');

btnPrevtagbar.onclick = function() {
   
    let index = 1;
    if(index*500 < tagbar__list.clientWidth) 
    {
        tagbar__list.style.left = `-${index*400 }px`;
        index++
    }
    btnNexttagbar.classList.remove("hidden");
    
}

btnNexttagbar.onclick = function() {
    tagbar__list.style.left = "60px";
    btnNexttagbar.classList.add("hidden");
}


console.log(btnPrevtagbar);
console.log(btnNexttagbar);
